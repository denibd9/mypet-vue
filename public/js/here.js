
if(navigator.geolocation){
	navigator.geolocation.getCurrentPosition(position => {
        localCoord = position.coords;
    alert('ok0')
        
		objLocalCoord = {
			lat: localCoord.latitude,
			lng: localCoord.longitude
		}
		var platform = new H.service.Platform({
  			'apikey': window.hereApiKey
			});
		// Obtain the default map types from the platform object:
		let defaultLayers = platform.createDefaultLayers();

        // Instantiate (and display) a map object:
       // if(document.getElementById('mapContainer')){
            let map = new H.Map(
                document.getElementById('mapContainer'),
                defaultLayers.vector.normal.map,
                {
                  zoom: 10,
                  center: objLocalCoord,
                  pixelRatio: window.devicePixelRation || 1
                });
                window.addEventListener('resize', () => map.getViewPort().resize());
            let ui = H.ui.UI.createDefault(map, defaultLayers);
            let mapEvents = new H.mapevents.MapEvents(map);
            let behavior = new H.mapevents.Behavior(mapEvents);
        //}
		function addDragableMarker(map, behavior){
            //alert('map')
			let inputLat = document.getElementById('lat');
			let inputLng =  document.getElementById('lng');
			if (inputLat.value != '' && inputLng.value != '') {
                objLocalCoord = {
                    lat: inputLat.value,
                    lng: inputLng.value
                }
            }
            svgStartMark = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve" width="512px" height="512px"><g><path d="M38.853,5.324L38.853,5.324c-7.098-7.098-18.607-7.098-25.706,0h0  C6.751,11.72,6.031,23.763,11.459,31L26,52l14.541-21C45.969,23.763,45.249,11.72,38.853,5.324z M26.177,24c-3.314,0-6-2.686-6-6  s2.686-6,6-6s6,2.686,6,6S29.491,24,26.177,24z" data-original="#1081E0" class="active-path" data-old_color="#1081E0" fill="#C12020"/></g> </svg>`;

            iconStart = new H.map.Icon(svgStartMark, {
                size: { h: 45, w: 45 }
            });
            let marker = new H.map.Marker(objLocalCoord, {
                volatility: true
            }, { icon: iconStart })

            marker.draggable = true;
            map.addObject(marker);

            map.addEventListener('dragstart', function(ev) {
                let target = ev.target,
                    pointer = ev.currentPointer;
                if (target instanceof H.map.Marker) {
                    let targetPosition = map.geoToScreen(target.getGeometry());
                    target['offset'] = new H.math.Point(
                        pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y
                    );
                    behavior.disable();
                }
            }, false);

            map.addEventListener('drag', function(ev) {
                let target = ev.target,
                    pointer = ev.currentPointer;
                if (target instanceof H.map.Marker) {
                    target.setGeometry(
                        map.screenToGeo(
                            pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y
                        )
                    );
                }
            }, false);

            map.addEventListener('dragend', function(ev) {
                let target = ev.target;
                if (target instanceof H.map.Marker) {
                   behavior.enable();
                   let resultCoord = map.screenToGeo(
                       ev.currentPointer.viewportX,
                       ev.currentPointer.viewportY
                   );
                   inputLat.value = resultCoord.lat.toFixed(50);
                   inputLng.value = resultCoord.lng.toFixed(50);
                }
            }, false);
        }
        alert('ok1')
        console.log('w',window.action)
        console.log('w', windowStatus)

		// if(window.action == "submit" || window.action_form == "submit" || windowStatus.action == "submit" || windowStatus.action_form == "submit"){
            if( windowStatus.action == "submit" || windowStatus.action_form == "submit"){
             alert('ok1')
            //if(window.action_form == "submit" || windowStatus.action_form == "submit")
            if(windowStatus.action_form == "submit")
            {
                let defaultLayers = platform.createDefaultLayers();

                // Instantiate (and display) a map object:
                let map_form = new H.Map(
                    document.getElementById('mapContainer_form'),
                    defaultLayers.vector.normal.map,
                    {
                    zoom: 10,
                    center: objLocalCoord,
                    pixelRatio: window.devicePixelRation || 1
                    });
                
                //window.addEventListener('resize', () => map.getViewPort().resize());
                let ui = H.ui.UI.createDefault(map_form, defaultLayers);
                let mapEvents_form = new H.mapevents.MapEvents(map_form);
                let behavior_form = new H.mapevents.Behavior(mapEvents_form);
                    addDragableMarker(map_form, behavior_form);
            }
        
            addDragableMarker(map, behavior);
		 }

        //cari lokasi codespace
        let spaces = [];
         const fetchSpaces = function (latitude, longitude, radius) {
         return new Promise(function(resolve, reject){
                resolve(
                     //fetch(`/api/spaces?lat=${latitude}&lng=${longitude}&rad=${radius}`)
                     fetch(`/api/groomingfilter?lat=${latitude}&lng=${longitude}&rad=${radius}`)
                    .then((res) => res.json())
                    .then(function(data) {
                        console.log('data',data)
                        svgEndMark = `<svg id="Capa_1" enable-background="new 0 0 497 497" height="512" viewBox="0 0 497 497" width="512" xmlns="http://www.w3.org/2000/svg"><g><path d="m426.202 263.073-34.459-6c-13.313 17.064-16.651 35.252-19.417 50.307-4.346 23.696-6.935 41.266-56.595 61.78-14.491 5.979-21.398 22.582-15.409 37.075 5.993 14.517 22.608 21.384 37.085 15.412 91.743-37.896 85.85-90.461 93.871-118.408z" fill="#5c586f"/><path d="m325.373 343.235h-226.018c-18.359 33.062-30.634 83.402 13.924 136.644 9.112 10.887 22.63 17.12 36.828 17.12h124.513c14.197 0 27.716-6.233 36.828-17.12 44.559-53.242 32.283-103.582 13.925-136.644z" fill="#6a647f"/><path d="m409.41 32.546c-1.779-10.492-8.851-19.275-18.723-23.251-41.565-16.742-90.706-11.13-128.089 18.971l100.954 125.375c37.384-30.102 53.348-76.915 45.858-121.095z" fill="#5c586f"/><path d="m337.177 129.999c25.714-20.704 38.077-52.778 33.552-84.677-10.403-3.867-21.258-5.781-32.065-5.781-20.564 0-40.966 6.925-57.821 20.498z" fill="#ffbfc5"/><path d="m15.318 32.546c1.779-10.492 8.851-19.275 18.723-23.251 41.565-16.742 90.706-11.13 128.089 18.971l-100.955 125.375c-37.383-30.102-53.347-76.915-45.857-121.095z" fill="#4f4c5f"/><path d="m87.551 129.999c-25.714-20.704-38.077-52.778-33.552-84.677 10.403-3.867 21.258-5.781 32.065-5.781 20.564 0 40.966 6.925 57.821 20.498z" fill="#ffbfc5"/><path d="m111.938 414.28c0-26.014 7.941-50.333 21.727-71.045h-34.31c-18.359 33.062-30.634 83.401 13.924 136.644 7.303 8.726 17.438 14.452 28.483 16.385-18.73-22.854-29.824-51.235-29.824-81.984z" fill="#5c586f"/><path d="m325.373 343.235c-31.802 18.437-70.83 29.302-113.009 29.302s-81.207-10.865-113.009-29.302c13.457-24.24 30.186-39.191 30.186-39.191h165.646s16.729 14.951 30.186 39.191z" fill="#64d1e1"/><path d="m172.909 304.044h-43.368s-16.729 14.951-30.186 39.191c8.417 4.879 17.344 9.222 26.698 12.98 10.66-20.766 26.855-38.685 46.856-52.171z" fill="#00c8de"/><circle cx="212.364" cy="171.72" fill="#6a647f" r="156.094"/><path d="m223.532 156.484h-19.144c-32.184 0-58.275 26.09-58.275 58.275 0 32.184 26.09 58.275 58.275 58.275h19.144c32.184 0 58.275-26.09 58.275-58.275-.001-32.185-26.091-58.275-58.275-58.275z" fill="#fff"/><g fill="#423e4f"><path d="m129.543 143.376c-4.143 0-7.5 3.358-7.5 7.5v16.18c0 4.142 3.357 7.5 7.5 7.5s7.5-3.358 7.5-7.5v-16.18c0-4.142-3.358-7.5-7.5-7.5z"/><path d="m297.946 143.376c-4.143 0-7.5 3.358-7.5 7.5v16.18c0 4.142 3.357 7.5 7.5 7.5s7.5-3.358 7.5-7.5v-16.18c0-4.142-3.358-7.5-7.5-7.5z"/><path d="m245.916 180.863c-4.143 0-7.5 3.358-7.5 7.5 0 4.669-3.799 8.468-8.469 8.468-4.669 0-8.468-3.799-8.468-8.468 0-4.142-3.357-7.5-7.5-7.5s-7.5 3.358-7.5 7.5c0 4.669-3.799 8.468-8.468 8.468-4.67 0-8.469-3.799-8.469-8.468 0-4.142-3.357-7.5-7.5-7.5s-7.5 3.358-7.5 7.5c0 12.94 10.528 23.468 23.469 23.468 6.163 0 11.776-2.392 15.968-6.292 4.192 3.899 9.804 6.292 15.968 6.292 12.94 0 23.469-10.528 23.469-23.468 0-4.142-3.358-7.5-7.5-7.5z"/></g><circle cx="212.364" cy="376.602" fill="#f9d888" r="21.36"/><path d="m212.364 431.116c-4.143 0-7.5 3.358-7.5 7.5v58.384h15v-58.384c0-4.142-3.358-7.5-7.5-7.5z" fill="#5c586f"/><path d="m183.812 497c-21.273-24.72-25.221-58.822-25.26-59.185-.441-4.114-4.13-7.093-8.249-6.657-4.118.438-7.104 4.131-6.666 8.25.151 1.422 3.5 31.111 21.34 57.592z" fill="#5c586f"/><path d="m481.718 238.156c-5.421-14.714-21.752-22.239-36.459-16.806-26.801 9.898-43.176 22.472-53.515 35.723l39.535 46.167c1.086-3.804 2.43-7.158 4.322-9.949 4.751-7.029 14.345-13.141 29.319-18.664 14.707-5.433 22.232-21.764 16.798-36.471z" fill="#dadada"/><g fill="#5c586f"><path d="m274.434 431.165c-4.099-.443-7.81 2.537-8.258 6.65-.038.347-3.983 34.459-25.261 59.186h18.835c17.84-26.48 21.19-56.169 21.341-57.592.437-4.116-2.542-7.802-6.657-8.244z"/><path d="m101.355 175.244c0-58.797 21.78-111.924 56.839-149.957-59.506 22.022-101.924 79.272-101.924 146.434 0 66.615 41.734 123.474 100.479 145.883-34.214-34.91-55.394-84.371-55.394-142.36z"/></g></g></svg>`;
                        iconEnd = new H.map.Icon(svgEndMark, {
                            size: { h: 45, w: 45 }
                        });
                        data.grooming.forEach(function (value, index) {
                            console.log('value',value)
                            let marker = new H.map.Marker({
                                lat: value.get_user_grooming.get_space.latitude, lng: value.get_user_grooming.get_space.longitude
                            }, { icon: iconEnd });
                            spaces.push(marker);
                        })
                    })

                )
            })
        }
        function clearSpace() {
            map.removeObjects(spaces);
            spaces = [];
        }

        function init(latitude, longitude, radius) {
            clearSpace();
            fetchSpaces(latitude, longitude, radius)
            .then(function () {
                map.addObjects(spaces);
                console.log('spaces', map)
            });
        }
       // console.log('ac', window.action)
        //if (window.action == 'browse' || windowStatus.action == 'browse') 
        if (windowStatus.action == 'browse') 
        {
            map.addEventListener('dragend', function (ev) {
                let resultCoord = map.screenToGeo(
                    ev.currentPointer.viewportX,
                    ev.currentPointer.viewportY
                );
                init(resultCoord.lat, resultCoord.lng, 40);
            }, false);

            init(objLocalCoord.lat, objLocalCoord.lng, 40);
        }
		//init(-6.949345, 107.6216236)
        // Route to space
        let urlParams = new URLSearchParams(window.location.search);

        function calculateRouteAtoB (platform) {
            let router = platform.getRoutingService(),
                routeRequestParam = {
                    mode: 'fastest;car',
                    representation: 'display',
                    routeattributes : 'summary',
                    maneuverattributes: 'direction,action',
                    waypoint0: urlParams.get('from'),
                    waypoint1: urlParams.get('to')
                }

            router.calculateRoute(
                routeRequestParam,
                onSuccess,
                onError
            )
        }

        function onSuccess(result) {
            route = result.response.route[0];

            addRouteShapeToMap(route);
            addSummaryToPanel(route.summary);
        }

        function onError(error) {
            alert('Can\'t reach the remote server' + error);
        }

        function addRouteShapeToMap(route){
            let linestring = new H.geo.LineString(),
                routeShape = route.shape,
                startPoint, endPoint,
                polyline, routeline, svgStartMark, iconStart, startMarker, svgEndMark, iconEnd, endMarker;

            routeShape.forEach(function(point) {
                let parts = point.split(',');
                linestring.pushLatLngAlt(parts[0], parts[1]);
            });

            startPoint = route.waypoint[0].mappedPosition;
            endPoint = route.waypoint[1].mappedPosition;

            polyline = new H.map.Polyline(linestring, {
                style: {
                lineWidth: 5,
                strokeColor: 'rgba(0, 128, 255, 0.7)',
                lineTailCap: 'arrow-tail',
                lineHeadCap: 'arrow-head'
                }
            });

            routeline = new H.map.Polyline(linestring, {
                style: {
                    lineWidth: 5,
                    fillColor: 'white',
                    strokeColor: 'rgba(255, 255, 255, 1)',
                    lineDash: [0, 2],
                    lineTailCap: 'arrow-tail',
                    lineHeadCap: 'arrow-head'
                }
            });

            svgStartMark = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve" width="512px" height="512px"><g><path d="M38.853,5.324L38.853,5.324c-7.098-7.098-18.607-7.098-25.706,0h0  C6.751,11.72,6.031,23.763,11.459,31L26,52l14.541-21C45.969,23.763,45.249,11.72,38.853,5.324z M26.177,24c-3.314,0-6-2.686-6-6  s2.686-6,6-6s6,2.686,6,6S29.491,24,26.177,24z" data-original="#1081E0" class="active-path" data-old_color="#1081E0" fill="#C12020"/></g> </svg>`;

            iconStart = new H.map.Icon(svgStartMark, {
                size: { h: 45, w: 45 }
            });

            startMarker = new H.map.Marker({
                lat: startPoint.latitude,
                lng: startPoint.longitude
            }, { icon: iconStart });

            svgEndMark = `<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve"> <path style="fill:#1081E0;" d="M38.853,5.324L38.853,5.324c-7.098-7.098-18.607-7.098-25.706,0h0 C6.751,11.72,6.031,23.763,11.459,31L26,52l14.541-21C45.969,23.763,45.249,11.72,38.853,5.324z M26.177,24c-3.314,0-6-2.686-6-6 s2.686-6,6-6s6,2.686,6,6S29.491,24,26.177,24z"/></svg>`;

            iconEnd = new H.map.Icon(svgEndMark, {
                size: { h: 45, w: 45 }
            });

            endMarker = new H.map.Marker({
                lat: endPoint.latitude,
                lng: endPoint.longitude
            }, { icon: iconEnd });


            // Add the polyline to the map
            map.addObjects([polyline, routeline, startMarker, endMarker]);

            // And zoom to its bounding rectangle
            map.getViewModel().setLookAtData({
                bounds: polyline.getBoundingBox()
            });
        }

        function addSummaryToPanel(summary){
            const sumDiv = document.getElementById('summary');
            const markup = `
                <ul>
                    <li>Total Jarak: ${summary.distance/1000}Km</li>
                    <li>Waktu Tempuh: ${summary.travelTime.toMMSS()} (Traffic Via Mobil)</li>
                </ul>
            `;
            sumDiv.innerHTML = markup;
        }

        // if (window.action == "direction" || windowStatus.action == "direction") {
            if (windowStatus.action == "direction") {
            calculateRouteAtoB(platform);

            Number.prototype.toMMSS = function () {
                return  Math.floor(this / 60)  +' minutes '+ (this % 60)  + ' seconds.';
            }
        }
	})
    // Open url direction
    function openDirection(lat, lng, id) {
        window.open(`/space/${id}?from=${objLocalCoord.lat},${objLocalCoord.lng}&to=${lat},${lng}`, "_self");
    }
}else{
	console.error("Geolocation is not supported by thus browser");
}

