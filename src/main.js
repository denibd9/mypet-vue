import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VuePageTransition from 'vue-page-transition'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import '@fortawesome/fontawesome-free/css/all.css' // Ensure you are using css-loader
import Vuetify from 'vuetify/lib'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueSweetalert2);
//import 'vue-toast-notification/dist/theme-sugar.css';

// import Bootstrap from 'bootstrap'; 
// import from 'bootstrap/dist/css/bootstrap.min.css';

Vue.use(Vuetify)
Vue.use(VuePageTransition)
Vue.use(BootstrapVue)
Vue.use(library)
Vue.use(faUserSecret)
Vue.use(FontAwesomeIcon)
//var Vue = require('vue');
var VueCookie = require('vue-cookie');

Vue.use(VueToast);
Vue.use(VueCookie);
Vue.config.productionTip = false

  new Vue({
  router,
  data: {
    apiConfig : 'http://localhost:8000/',
    auth: false,
    maximum: 50,
    pets: [],
    petJenis: [],
    authUser: {},
  },
  method: {
    convertToRupiah(angka)
    {
      var rupiah = '';    
      var angkarev = angka.toString().split('').reverse().join('');
      for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
      return ' '+rupiah.split('',rupiah.length-1).reverse().join('');
    }
  },
  
  render: function (h) { return h(App) }
}).$mount('#app')