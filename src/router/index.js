import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Kategori from '../views/Kategori.vue'
import MyMarket from '../views/MyMarket.vue'
import PetDetail from '../views/PetDetail.vue'
import MyComunity from '../views/MyComunity.vue'
import MyComunityId from '../views/MyComunityId.vue'
import Profil from '../views/Profil.vue'
import MitraMarket from '../views/MitraMarket.vue'
import Notif from '../views/Notif.vue'
import MyCargo from '../views/MyCargo.vue'
import Master from '../views/Master.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/kategori',
    name: 'Kategori',
    component: Kategori,
    meta: { transition: 'flip-y' },
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    // component: function () {
    //   return import(/* webpackChunkName: "about" */ '../views/About.vue')
    // }
  },
  {
    path: '/mymarket',
    name: 'MyMarket',
    component: MyMarket,
     meta: { transition: 'flip-y' },
  },
  {
    path: '/comunity',
    name: 'MyComunity',
    component: MyComunity,
     meta: { transition: 'flip-y' },
  },
  {
    path: '/comunity_:id',
    name: 'MyComunityId',
    component: MyComunityId,
    meta: { transition: 'fade-in-right' },
  },
  {
    path: '/pet_:id',
    name: 'PetDetail',
    component: PetDetail,
    meta: { transition: 'fade-in-right' }
  },
  {
    path: '/mitra_market',
    name: 'MitraMarket',
    component: MitraMarket,
    meta: { transition: 'flip-y' },
  },
  {
    path: '/notif',
    name: 'Notif',
    component: Notif,
     meta: { transition: 'flip-y' },
  },
  {
    path: '/mycargo',
    name: 'MyCargo',
    component: MyCargo,
     meta: { transition: 'flip-y' },
  },
  {
    path: '/:username',
    name: 'Profil',
    component: Profil,
    meta: { transition: 'flip-y' },
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
